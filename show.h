#ifndef SHOW_H_
#define SHOW_H_

#include <stdlib.h>
#include <inttypes.h>
#include <avr/interrupt.h>

#include "effects.h"
#include "globals.h"
#include "hsv_rgb.h"
#include "c_tast.h"
#include "TLC5940.h"
#include "lib_dmx_in.h"
#include "status_led.h"
#include "led_stripes.h"

void showIsr10Hz ( void );
void showIsr1Hz ( void );
void handleShow( void );

#endif /* SHOW_H_ */
