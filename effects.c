/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file effects.c
 *
 *  This file implements effect routines
 *
 * \author Jonathan Krieger
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#include "effects.h"

uint8_t stripe_nr;
uint8_t timer_for_effect_isr_10Hz = 0;

// Table[(e^(n/8)-1)×87, {n, 0, 31}] calculated@: http://www.wolframalpha.com/input/?i=Table[%28e**[n%2F8]-1%29*87%2C+{n%2C+0%2C+31}]
// last value changed to 4095 (the max. of the pwm value the TLC5940 chip can handle for each channel)
const uint16_t brightness_5_to_12bit[32] = {0, 11, 24, 39, 56, 75, 97, 121, 149, 180, 216, 257, 302, 354, 413, 480, 555, 641, 738, 848, 972, 1114, 1273, 1455, 1660, 1893, 2156, 2455, 2794, 3177, 3612, 4095};

void effectIsr100Hz(void)
{
    if ( !color_update_flow )
    {
        for (stripe_nr=0; stripe_nr<20; stripe_nr++)
        {
            color_counter_flow[stripe_nr] += speed_5bit;
            if ( color_counter_flow[stripe_nr] > 4095 )
            {
                color_counter_flow[stripe_nr] = 0;
            }
            color_struc_flow[stripe_nr].H = color_counter_flow[stripe_nr];
        }
        color_update_flow = 1;
    }
}

void effectIsr10Hz(void)
{
 cli();
    timer_for_effect_isr_10Hz++;
    if (timer_for_effect_isr_10Hz > 1)
    {
        timer_for_effect_isr_10Hz = 0;

        if (handle_rand)
        {
            handle_rand = 0;
        }
    }
    timer2_for_effect_isr_10Hz++;
    if (timer2_for_effect_isr_10Hz > 19)
    {
        timer_for_effect_isr_10Hz = 0;
        switch (rand() % 3)
        {
        case 0:
            colorWalkR = rand() % 4095;
            colorWalkR = rand() % 4095;
            colorWalkB = 0;
            break;

        case 1:
            colorWalkR = rand() % 4095;
            colorWalkR = 0;
            colorWalkB = rand() % 4095;
            break;

        case 2:
            colorWalkR = 0;
            colorWalkR = rand() % 4095;
            colorWalkB = rand() % 4095;
            break;
        }
    }
    sei();
}

void initEffects( void )
{
    mode = 0; ///mode to start with
    hue_step = 1;
    button_pressed_long_previous_500ms = 0;
    button_pressed_since_s = 0;
    status_mode = 0;

    brightness_5bit = 12;
    saturation_5bit = 31;
    brightness_12bit = 1000;
    brightness_12bit_green = 1700;
    brightness_12bit_blue = 200;
    speed_5bit = 10;
    mode_white = 0;
    mode_stage_fx = 0;
    mode_moving_fx = 3;
    color_counter_single = 0;
    color_struc_single.H = 0;
    color_struc_single.S = 4095;
    color_struc_single.V = 4095;
    handle_rand = 0;

    colorWalkR = 0;
    colorWalkG = 0;
    colorWalkB = 0;

    for (stripe_nr=0; stripe_nr<20; stripe_nr++)
    {
        color_counter_flow[stripe_nr] = stripe_nr*215;
        color_struc_flow[stripe_nr].H = 0;
        color_struc_flow[stripe_nr].S = 4095;
        color_struc_flow[stripe_nr].V = 4095;
    }
    color_update_flow = 1;
}

void increaseBrightness ( void )
{
    cli();
    ++brightness_5bit;
    if(brightness_5bit > 31)
    {
        brightness_5bit = 0;
        if (mode_white)
        {
            mode_white = 0; // warm white
        }
        else
        {
            mode_white = 1; // cool white
        }
    }
    brightness_12bit = brightness_5_to_12bit[brightness_5bit];
    brightness_12bit_green = brightness_12bit * 0.41;
    brightness_12bit_blue = brightness_12bit * 0.05;
    color_struc_single.V = brightness_12bit;
    status_led_second_mode = brightness_5bit;
    statusLeds5Bit(status_led_second_mode);
    sei();
}

void increaseHue ( uint8_t amount )
{
    cli();
    color_struc_single.H += amount;
    if(color_struc_single.H > 4095)
    {
        color_struc_single.H = 0;
    }
    hsv_to_rgb12( &color_struc_single );
    status_led_second_mode = amount;
    statusLeds5Bit( amount );
    //status_led_second_mode = color_struc_single.H/132; see show.c
    //statusLeds5Bit(status_led_second_mode);
    sei();
}

void increaseSaturation ( uint8_t amount )
{
    cli();
    color_struc_single.S += amount;
    if(color_struc_single.S > 4095)
    {
        color_struc_single.S = 0;
    }
    hsv_to_rgb12( &color_struc_single );
    status_led_second_mode = color_struc_single.S/132;
    statusLeds5Bit(status_led_second_mode);
    sei();
}

void increaseSpeed ( void )
{
    cli();
    ++speed_5bit;
    if(speed_5bit > 31)
    {
        speed_5bit = 0;
    }
    status_led_second_mode = speed_5bit;
    statusLeds5Bit(status_led_second_mode);
    sei();
}

void handleWhite ( void )
{
    if (mode_white)
    {
        setAllLeds(brightness_12bit,brightness_12bit,brightness_12bit);
    }
    else
    {
        setAllLeds(brightness_12bit,brightness_12bit_green,brightness_12bit_blue);
    }
    TLC5940_display();
}

void handleSingleColor ( void )
{
    hsv_to_rgb12( &color_struc_single );
    setAllLeds( color_struc_single.Pin_R, color_struc_single.Pin_G, color_struc_single.Pin_B );
    TLC5940_display();
}

void handleStageMode ( void )
{
    Stage(mode_stage_fx);
}

void handleMovingMode ( void )
{
    switch (mode_moving_fx)
    {
    case 0:
        handleWaveV();
        break;

    case 1:
        handleRainbow();
        break;

    case 2:
        handleWaveH();
        break;

    case 3:
        handleWaveVRand();
        break;

    case 4:
        handleRand2();
        break;
    }
}

void switchStageMode ( void )
{
    ++mode_stage_fx;
    if(mode_stage_fx > 1)
    {
        mode_stage_fx = 0;
    }
    status_led_second_mode = mode_stage_fx;
    statusLeds5Bit(status_led_second_mode);
}


void switchMovingMode ( void )
{
    ++mode_moving_fx;
    if(mode_moving_fx > 4)
    {
        mode_moving_fx = 0;
    }
    status_led_second_mode = mode_moving_fx;
    statusLeds5Bit(status_led_second_mode);
}


