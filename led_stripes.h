/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file led_stripes.h
 *
 *  This module provides led stripes controll routines
 *
 * \author Jonathan Krieger
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#ifndef LED_STRIPES_H_
#define LED_STRIPES_H_

#include <inttypes.h>
#include "TLC5940.h"

#define NR_OF_STRIPES 20

void setAllLeds( uint16_t red, uint16_t green, uint16_t blue );
void setLedRgb( uint8_t nr, uint16_t red, uint16_t green, uint16_t blue );
void setLed( uint8_t nr, uint16_t brightness );

const uint8_t stripe_conf[NR_OF_STRIPES][3];

#endif /* LED_STRIPES_H_ */
