/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file hsv_rgb.h
 *
 *  This module provides hsv to rgb conversion routines
 *
 * \author Frank
 *
 * \remarks Code based on code from http://www.mikrocontroller.net/topic/237473
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#ifndef HSV_RGB_H_
#define HSV_RGB_H_

#include <stdint.h>

typedef struct _colorstruc
{
    uint16_t H;
    uint16_t S;
    uint16_t V;
    uint16_t Pin_R;
    uint16_t Pin_G;
    uint16_t Pin_B;
} colorstruc;

extern void hsv_to_rgb8(colorstruc *value);
extern void hsv_to_rgb12(colorstruc *value);

#endif /* HSV_RGB_H_ */
