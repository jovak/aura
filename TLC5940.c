/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file TLC5940.c
 *
 *  This file implements the communication routines for one or more TLC5940 chips
 *
 * \author Frank
 *
 * \remarks Code based on code from http://www.mikrocontroller.net/topic/237473
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#include <avr/interrupt.h>
#include <stdlib.h>
#include "TLC5940.h"

// AVR pins:
#define TLCPORT		PORTB  // PORTB
#define TLCDDR		DDRB    // MEGA  TLC  //DDRB
#define SCLK_PIN	PB0		//  14    25  //PB0
#define XLAT_PIN	PB1     //  15    24  //PB1
#define BLANK_PIN	PB2     //  16    23  //PB2
#define GSCLK_PIN	PB3     //  17    18  //PB3
#define SIN_PIN		PB4     //  18    26  //PB4

#define TRUE		1
#define FALSE		0

#define numdrivers  4

uint8_t shiftbits;
uint8_t frame[numdrivers*24];

static volatile uint8_t tlc5940_needpulse = FALSE;
static volatile uint8_t tlc5940_transferdone = FALSE;

void TLC5940_init( uint8_t resolutionbits )
{
    shiftbits = 12 - resolutionbits;
    TLC5940_clear();
    TLC5940_portInit();
    TLC5940_timerInit();
    tlc5940_transferdone = FALSE;
    TLC5940_display();
}

static void shift8( uint8_t v )
{
    for (uint8_t i=0x80; i; i>>=1)
    {
        if ( v & i )
            TLCPORT |= _BV(SIN_PIN);
        else
            TLCPORT &= ~_BV(SIN_PIN);
        TLCPORT |= _BV(SCLK_PIN);
        TLCPORT &= ~_BV(SCLK_PIN);
    }
}

void TLC5940_portInit( void )
{
    // Set pins to output
    TLCDDR |= _BV(BLANK_PIN) | _BV(XLAT_PIN) | _BV(SCLK_PIN) | _BV(SIN_PIN) | _BV(GSCLK_PIN);

    TLCPORT &= ~_BV(BLANK_PIN);             // blank everything until ready
    TLCPORT &= ~_BV(XLAT_PIN);
    TLCPORT &= ~_BV(SCLK_PIN);
    TLCPORT &= ~_BV(GSCLK_PIN);
}

void TLC5940_timerInit( void )
{
    // PWM timer2
    TCCR2A = ( _BV(WGM21) |                 // CTC
               _BV(COM2A0) );               // toggle OC2A on match -> GSCLK
    TCCR2B = _BV(CS20);                     // No prescaler
    OCR2A = 1;                              // toggle every timer clock cycle -> 4 MHz
    TCNT2 = 0;

    // Latch timer0
    TCCR0A = ( _BV(WGM01) | _BV(WGM00) );   // Fast PWM 8-bit
    TCCR0B = ( _BV(CS01) |                  // 64 prescaler     =>
               _BV(CS00) );                 // Fast PWM 8-bit  =>  1/4096th of OC2A
    TIMSK0 = _BV(TOIE0);                    // Enable overflow interrupt
    TCNT0 = 0;
}

void TLC5940_clear( void )
{
    uint8_t i;
    for ( i = 0; i < numdrivers * 24; i++ )
    {
        frame[i] = 0;
    }
}

void TLC5940_setLED(uint8_t lednr, uint16_t intensity)
{
    uint16_t bitnr = lednr * 12; // 12 bits per led
    uint8_t bytenr = bitnr >> 3;
    uint8_t startnibble = (bitnr & 7)?TRUE:FALSE;

    intensity <<= shiftbits;

    if (!startnibble)
    {
        frame[bytenr] = intensity >> 4;
        frame[bytenr+1] = ( frame[bytenr + 1] & 0x0f) | ( ( intensity & 0x0f ) << 4 );
    }
    else
    {
        frame[bytenr] = ( frame[bytenr] & 0xf0 ) | ( intensity >> 8 );
        frame[bytenr+1] = intensity & 0xff;
    }
}

void TLC5940_display( void )
{
    uint8_t i;
    while ( tlc5940_transferdone ) {}

    for ( i = 0; i < numdrivers*24; i++ )
    {
        shift8(frame[i]);
    }
    tlc5940_transferdone = TRUE;
}

ISR(TIMER0_OVF_vect)
{
    TLCPORT |= _BV(BLANK_PIN);

    // Stop timers
    TCCR0B &= ~_BV(CS01);
    TCCR2B &= ~_BV(CS20);

    // Latch only if new data is available
    if ( tlc5940_transferdone )
    {
        TLCPORT |= _BV(XLAT_PIN); // latch
        TLCPORT &= ~_BV(XLAT_PIN);
        tlc5940_transferdone = FALSE;

        // Extra SCLK pulse according to Datasheet p.18
        if (tlc5940_needpulse)
        {
            TLCPORT |= _BV(SCLK_PIN);
            TLCPORT &= ~_BV(SCLK_PIN);
            tlc5940_needpulse = FALSE;
        }
    }

    TLCPORT &= ~_BV(BLANK_PIN);

    // Restart timers
    TCNT2 = 0;
    TCNT0 = 0;
    TCCR0B |= _BV(CS01);
    TCCR2B |= _BV(CS20);
}
