/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file status_led.h
 *
 *  This module provides status led controll routines
 *
 * \author Jonathan Krieger
 *
 * \remarks
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#ifndef STATUS_LED_H_
#define STATUS_LED_H_

#include <avr/io.h>

#define LED_DDR    DDRC
#define LED_PORT   PORTC
#define ALL_LEDS   (1<<0 | 1<<1 | 1<<2 | 1<<3 | 1<<4 | 1<<5)

void initStatusLeds ( void );
void statusLedsOff ( void );
/*
void statusLedsOn ( void );
void statusLedOff ( uint8_t nr );
void statusLedOn ( uint8_t nr );*/
void statusLeds5Bit ( uint8_t nr );

volatile uint8_t status_led_second_mode;

#endif /* STATUS_LED_H_ */
