/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file moving_fx.c
 *
 *  This file implements effect routines
 *
 * \author Jonathan Krieger
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#include "moving_fx.h"

void handleRainbow( void )
{
    uint8_t i;
    if ( color_update_flow )
    {

        for (i=0; i<20; i++)
        {
            color_struc_flow[i].V = brightness_12bit;
            hsv_to_rgb12( &color_struc_flow[i] );
            setLedRgb(i, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
        }
        TLC5940_display();
        color_update_flow = 0;
    }
}

void handleWaveV( void )
{
    uint8_t i;
    if ( color_update_flow )
    {
        for (i=0; i<5; i++)
        {
            color_struc_flow[i].V = brightness_12bit;
            hsv_to_rgb12( &color_struc_flow[i] );
            setLedRgb(i, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
            setLedRgb(i+5, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
            setLedRgb(i+10, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
            setLedRgb(i+15, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
        }
        TLC5940_display();
        color_update_flow = 0;
    }
}

void handleWaveH( void )
{
    uint8_t i;
    if ( color_update_flow )
    {
        for (i=0; i<4; i++)
        {
            color_struc_flow[i].V = brightness_12bit;
            hsv_to_rgb12( &color_struc_flow[i] );
            setLedRgb(i*5+0, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
            setLedRgb(i*5+1, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
            setLedRgb(i*5+2, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
            setLedRgb(i*5+3, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
            setLedRgb(i*5+4, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
        }
    if (handle_rand)
    {
        TLC5940_display();
    }
        color_update_flow = 0;
    }
}
/*
void handle2Waves( void )
{
    uint8_t i;
    if ( color_update_flow )
    {
        for (i=0; i<5; i++)
        {
            color_struc_flow[i].V = brightness_12bit;
            hsv_to_rgb12( &color_struc_flow[i] );
            hsv_to_rgb12( &color_struc_flow[i+9] );
            setLedRgb(i, color_struc_flow[i+9].Pin_R, color_struc_flow[i+9].Pin_G, color_struc_flow[i+9].Pin_B );
            setLedRgb(i+5, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
            setLedRgb(i+10, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
            setLedRgb(i+15, color_struc_flow[i].Pin_R, color_struc_flow[i].Pin_G, color_struc_flow[i].Pin_B );
        }
        TLC5940_display();
        color_update_flow = 0;
    }
}
*/
void handleWaveVRand( void )
{
    if (!handle_rand)
    {
        //handle2Waves();
        handleWaveV();
        handleRand2();
        handleWalk();
    }
}
/*
void handleRand( void )
{
    if (!handle_rand)
    {
        setLedRgb(rand() % 20, color_struc_flow[rand() % 4095].Pin_R, color_struc_flow[0].Pin_G, color_struc_flow[rand() % 4095].Pin_B );
        setLedRgb(rand() % 20, color_struc_flow[0].Pin_R, color_struc_flow[rand() % 4095].Pin_G, color_struc_flow[rand() % 4095].Pin_B );
        setLedRgb(rand() % 20, color_struc_flow[rand() % 4095].Pin_R, color_struc_flow[rand() % 4095].Pin_G, color_struc_flow[0].Pin_B );
        TLC5940_display();
        handle_rand = 1;
    }
}
*/
void handleRand2( void )
{
    if (!handle_rand)
    {
        uint16_t colorB1 = rand() % 4095;
        uint16_t colorB2 = rand() % 4095;
        uint8_t color = rand() % 3;
        uint8_t number = rand() % 17;
        switch (color)
        {
        case 0:
            setLedRgb(number, color_struc_flow[colorB1].Pin_R, color_struc_flow[0].Pin_G, color_struc_flow[colorB2].Pin_B );
            setLedRgb(number+1, color_struc_flow[colorB1].Pin_R, color_struc_flow[0].Pin_G, color_struc_flow[colorB2].Pin_B );
            setLedRgb(number+2, color_struc_flow[colorB1].Pin_R, color_struc_flow[0].Pin_G, color_struc_flow[colorB2].Pin_B );
            break;
        case 1:
            setLedRgb(number, color_struc_flow[0].Pin_R, color_struc_flow[colorB1].Pin_G, color_struc_flow[colorB2].Pin_B );
            setLedRgb(number+1, color_struc_flow[0].Pin_R, color_struc_flow[colorB1].Pin_G, color_struc_flow[colorB2].Pin_B );
            setLedRgb(number+2, color_struc_flow[0].Pin_R, color_struc_flow[colorB1].Pin_G, color_struc_flow[colorB2].Pin_B );
            break;
        case 2:
            setLedRgb(number, color_struc_flow[colorB1].Pin_R, color_struc_flow[colorB2].Pin_G, color_struc_flow[0].Pin_B );
            setLedRgb(number+1, color_struc_flow[colorB1].Pin_R, color_struc_flow[colorB2].Pin_G, color_struc_flow[0].Pin_B );
            setLedRgb(number+2, color_struc_flow[colorB1].Pin_R, color_struc_flow[colorB2].Pin_G, color_struc_flow[0].Pin_B );
            break;
        }
        //TLC5940_display();
        handle_rand = 1;
    }
}
void handleWalk ( void )
{
    setLedRgb(timer2_for_effect_isr_10Hz, color_struc_flow[colorWalkR].Pin_R, color_struc_flow[colorWalkG].Pin_G, color_struc_flow[colorWalkB].Pin_B );
    TLC5940_display();
}
