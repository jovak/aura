/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file main.c
 *
 *  This programm controlls 20 rgb led stripes.
 *  It recieves DMX and it has some routines to controll the stripes.
 *
 * \author Jonathan Krieger
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <avr/interrupt.h>

#include "TLC5940.h"
#include "timer.h"
#include "lib_dmx_in.h"
#include "status_led.h"
#include "show.h"
#include "effects.h"

int main( void )
{
    cli();

    TLC5940_init( 12 );
    initStatusLeds(); // Initialise status leds
    initEffects(); // Initialise effect variables
    initKeys(); // Initialise keys

    DmxAddress= 1; // Set DMX start adress
    init_DMX_RX(); // Initalise DMX reception

    sei();

    //handleWhite();
    handleSingleColor();
    timer_init (); // Initialise timer


    while(1)
    {
        handleShow();
    }
    return 0;
}
