/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file show.c
 *
 *  This programm controlls 20 rgb led stripes.
 *
 * \author Jonathan Krieger
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/
#include "show.h"


void showIsr10Hz ( void )
{
    if (button_pressed_long_previous_500ms > 0)
    {
        ++button_pressed_long_previous_500ms;
    }
    if (button_pressed_long_previous_500ms > 6)
    {
        button_pressed_long_previous_500ms = 0;
        hue_step = 1;
    }
}

void showIsr1Hz ( void )
{
    if (button_pressed_since_s > 0)
    {
        ++button_pressed_since_s;

        if (button_pressed_since_s > 12)
        {
            button_pressed_since_s = 0;
            statusLedsOff();
        }
        else if (button_pressed_since_s > 2)
        {
            status_mode = button_pressed_since_s % 3;
            switch(status_mode)
            {
            case 0:
                statusLedsOff();
                break;

            case 1:
                statusLeds5Bit( mode + 1 );
                break;

            case 2:
                statusLeds5Bit( status_led_second_mode );
                break;
            }
        }
    }
}
void handleDMX( void )
{
    if ( DmxFinishFlag == 1 )
    {
        uint8_t channel = 0;
        //uint8_t stripe_nr = 0;
        while (channel < 60)
        {
            cli();
            setLed( channel, DmxRxField[channel]*8 );
            //sedLedRgb( stripe_nr++, DmxRxField[channel]*8, DmxRxField[++channel]*8, DmxRxField[++channel]*8 );
            ++channel;
            sei();
        }
        TLC5940_display();
        DmxFinishFlag = 0;
    }
}

void handleShow( void )
{
    if( get_key_rpt ( 1<<KEY0 ) )
    {
        button_pressed_long_previous_500ms = 1;
        button_pressed_since_s = 1;
        switch(mode) // switch mode for long pressed
        {
        case 0:
            increaseBrightness();
            break;

        case 1:
            if(hue_step < 32)
            {
                ++hue_step;
            }
            increaseHue( hue_step );
            handleSingleColor();
            break;

        case 2:
            switchStageMode();
            break;

        case 3:
            switchMovingMode();
            break;

        case 4:
            increaseSpeed();
            break;
        }
    }
    else if( ( get_key_short( 1<<KEY0 ) ) && ( !button_pressed_long_previous_500ms ) )
    {
        ++mode;
        if( mode > 5 ) mode = 0;
        button_pressed_since_s = 1;
        //statusLedsOff();
        statusLeds5Bit( mode + 1 );
        switch(mode)
        {
        case 0:
            handleWhite();
            break;

        case 1:
            handleSingleColor();
            break;

        case 2:
            handleStageMode();
            break;

        case 3:
            handleMovingMode();
            break;

        case 4:
            handleMovingMode();
            break;

        case 5:
            //handleDMX2leds;
            break;
        }
    }
    switch(mode)
    {
        case 0:
            handleWhite();
            break;

        case 1:
            handleSingleColor();
            break;

        case 2:
            handleStageMode();
            break;

    case 3:
        handleMovingMode();
        break;

    case 4:
        handleMovingMode();
        break;

    case 5:
        handleDMX();
        break;
    }
}

