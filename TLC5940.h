/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file TLC5940.h
 *
 *  This module provides the communication routines for one or more TLC5940 chips
 *
 * \author Frank
 *
 * \remarks Code based on code from http://www.mikrocontroller.net/topic/237473
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#ifndef TLC5940_H_
#define TLC5940_H_

#include <stdint.h>

extern void TLC5940_portInit( void );
extern void TLC5940_init( uint8_t resolutionbits );
extern void TLC5940_timerInit( void );
extern void TLC5940_setLED( uint8_t lednr, uint16_t intensity );
extern void TLC5940_display( void );
extern void TLC5940_clear( void );

#endif /* TLC5940_H_ */
