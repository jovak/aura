/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file moving_fx.h
 *
 *  This module provides effect routines
 *
 * \author Jonathan Krieger
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#ifndef MOVING_FX_H_
#define MOVING_FX_H_

#include <stdlib.h>
#include <util/delay.h>
#include <inttypes.h>

#include "hsv_rgb.h"
#include "TLC5940.h"
#include "led_stripes.h"
#include "globals.h"
//#include "effects.h"
volatile uint8_t handle_rand;

void handleRainbow( void );
void handleWaveH( void );
void handleWaveV( void );
void handleWaveVRand( void );
/*void handleRand( void );*/
void handleRand2( void );

void handleWalk ( void );

#endif /* MOVING_FX_H_ */
