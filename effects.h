/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file effects.h
 *
 *  This module provides effect routines
 *
 * \author Jonathan Krieger
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#ifndef EFFECTS_H_
#define EFFECTS_H_

#include <stdlib.h>
#include <util/delay.h>
#include <inttypes.h>
#include <avr/interrupt.h>

#include "hsv_rgb.h"
#include "TLC5940.h"
#include "lib_dmx_in.h"
#include "led_stripes.h"
#include "status_led.h"
#include "globals.h"
#include "moving_fx.h"
#include "stage_fx.h"

void effectIsr100Hz(void);
void effectIsr10Hz(void);
void initEffects( void );
void increaseBrightness ( void );
void increaseHue ( uint8_t amount );
void increaseSaturation ( uint8_t amount );
void increaseSpeed ( void );
void handleWhite ( void );
void handleSingleColor ( void );
void handleStageMode ( void );
void handleMovingMode ( void );
void switchStageMode ( void );
void switchMovingMode ( void );

#endif /* EFFECTS_H_ */
