/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file status_led.c
 *
 *  This file implements status led controll routines
 *
 * \author Jonathan Krieger
 *
 * \remarks
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#include "status_led.h"

const uint8_t status_leds_5bit[32] = {0, 1, 2, 4, 8, 16, 32, 3, 6, 12, 24, 48, 33, 7, 14, 28, 56, 49, 35, 15, 30, 60, 57, 51, 39, 62, 61, 59, 55, 47, 31, 63};

void initStatusLeds ( void )
{
    LED_PORT |= ALL_LEDS; /* Status-LED-Pins "high" means off */
    LED_DDR |= ALL_LEDS; /* Status-LED-Pins "high" means output */
    LED_PORT &= ~(1<<0);
}

void statusLedsOff ( void )
{
    LED_PORT |= ALL_LEDS; // Status-LED-Pin "high" means off
}
/*
void statusLedsOn ( void )
{
    LED_PORT &= ~(ALL_LEDS); // Status-LED-Pin "low" means on
}

void statusLedOff ( uint8_t nr )
{
    switch(nr)
    {
    case 0:
        LED_PORT |= (1<<LED0);
        break;

    case 1:
        LED_PORT |= (1<<LED1);
        break;

    case 2:
        LED_PORT |= (1<<LED2);
        break;

    case 3:
        LED_PORT |= (1<<LED3);
        break;

    case 4:
        LED_PORT |= (1<<LED4);
        break;

    case 5:
        LED_PORT |= (1<<LED5);
        break;
    }
}

void statusLedOn ( uint8_t nr )
{
    switch(nr)
    {
    case 0:
        LED_PORT &= ~(1<<LED0);
        break;

    case 1:
        LED_PORT &= ~(1<<LED1);
        break;

    case 2:
        LED_PORT &= ~(1<<LED2);
        break;

    case 3:
        LED_PORT &= ~(1<<LED3);
        break;

    case 4:
        LED_PORT &= ~(1<<LED4);
        break;

    case 5:
        LED_PORT &= ~(1<<LED5);
        break;
    }
}*/

void statusLeds5Bit ( uint8_t value )
{
    //statusLedsOff();
    //LED_PORT &= ~( status_leds_5bit[value] );
    LED_PORT = ~( status_leds_5bit[value] );
}
