/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file hsv_rgb.c
 *
 *  This file implements hsv to rgb conversion routines
 *
 * \author Frank
 *
 * \remarks Code based on code from http://www.mikrocontroller.net/topic/237473
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#include "hsv_rgb.h"
/*
void hsv_to_rgb8(colorstruc *value)
{
    uint8_t r, g, b, i, f;
    uint16_t p, q, t;

    if( value->S == 0 )
    {
        r = g = b = value->V;
    }
    else
    {
        i = value->H / 43;
        f = value->H % 43;
        p = ( value->V * ( 255 - value->S ) ) / 256;
        q = ( value->V * ( ( 10710 - ( value->S * f ) ) / 42 ) ) / 256;
        t = ( value->V * ( ( 10710 - ( value->S * ( 42 - f ) ) ) / 42 ) ) / 256;

        switch( i )
        {
        case 0:
            value->Pin_R = value->V;
            value->Pin_G = t;
            value->Pin_B = p;
            break;

        case 1:
            value->Pin_R = q;
            value->Pin_G = value->V;
            value->Pin_B = p;
            break;

        case 2:
            value->Pin_R = p;
            value->Pin_G = value->V;
            value->Pin_B = t;
            break;

        case 3:
            value->Pin_R = p;
            value->Pin_G = q;
            value->Pin_B = value->V;
            break;

        case 4:
            value->Pin_R = t;
            value->Pin_G = p;
            value->Pin_B = value->V;
            break;

        default:
            value->Pin_R = value->V;
            value->Pin_G = p;
            value->Pin_B = q;
            break;
        }
    }
}
*/
void hsv_to_rgb12(colorstruc *value)
{
    uint16_t q;
    uint16_t p;
    uint16_t t;

    uint8_t i;
    uint16_t f;

    if( value->S == 0 )
    {
        value->Pin_R = value->V;
        value->Pin_G = value->V;
        value->Pin_B = value->V;
    }
    else
    {
        i = value->H / 683;
        f = value->H % 683;
        p = (uint16_t)( ( value->V * ( 4095 - value->S ) ) / 4096 );
        q = (uint16_t)( ( value->V * ( ( 2796885 - ( value->S * (uint32_t)f ) ) / 683 ) ) / 4096 );
        t = (uint16_t)( ( ( value->V * ( ( 2796885 - ( value->S * ( 683 - ( (uint32_t)f ) ) ) ) / 683 ) ) ) / 4096 );

        switch( i )
        {
        case 0:
            value->Pin_R = value->V;
            value->Pin_G = t;
            value->Pin_B = p;
            break;

        case 1:
            value->Pin_R = q;
            value->Pin_G = value->V;
            value->Pin_B = p;
            break;

        case 2:
            value->Pin_R = p;
            value->Pin_G = value->V;
            value->Pin_B = t;
            break;

        case 3:
            value->Pin_R = p;
            value->Pin_G = q;
            value->Pin_B = value->V;
            break;

        case 4:
            value->Pin_R = t;
            value->Pin_G = p;
            value->Pin_B = value->V;
            break;

        default:
            value->Pin_R = value->V;
            value->Pin_G = p;
            value->Pin_B = q;
            break;
        }
    }
}

