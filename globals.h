/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file globals.h
 *
 *  This module provides effect routines
 *
 * \author Jonathan Krieger
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "hsv_rgb.h"

volatile uint8_t mode_stage_fx; // Stage effects are effects, which have brightness focus on the stage
volatile uint8_t mode_moving_fx; // Moving effects

volatile uint8_t  brightness_5bit;
volatile uint8_t  saturation_5bit;
volatile uint16_t brightness_12bit;
volatile uint16_t brightness_12bit_green;
volatile uint16_t brightness_12bit_blue;
volatile uint8_t  speed_5bit;

colorstruc         color_struc_single;
volatile uint16_t  color_counter_single;

colorstruc         color_struc_flow[20];
volatile uint16_t  color_counter_flow[20];
volatile uint8_t   color_update_flow;

//colorstruc         color_struc_walk;
//volatile uint16_t  color_counter_walk;

volatile uint8_t timer2_for_effect_isr_10Hz;
volatile uint16_t colorWalkR;
volatile uint16_t colorWalkG;
volatile uint16_t colorWalkB;

volatile uint8_t mode_white;

volatile uint8_t mode;
volatile uint8_t hue_step;
volatile uint8_t button_pressed_long_previous_500ms;
volatile uint8_t button_pressed_since_s;
volatile uint8_t status_mode;

#endif /* GLOBALS_H_ */
