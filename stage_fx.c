/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file stage_fx.c
 *
 *  This file implements effect routines
 *
 * \author Jonathan Krieger
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#include "stage_fx.h"

// [stage_mode][stripe_nr][RGB]
const uint16_t stageModesFixeds[2][10][3] =
    { { { 4095, 1000, 0 },
        { 2000, 200, 0},
        { 200, 0, 200},
        { 50, 0, 100},
        { 0, 0, 100},

        { 4095, 1300, 0},
        { 1500, 0, 0},
        { 200, 0, 50},
        { 100, 0, 100},
        { 0, 0, 150} },

        { { 4095, 1000, 0 },
        { 2000, 200, 0},
        { 200, 200, 0},
        { 50, 100, 0},
        { 0, 100, 0},

        { 4095, 1300, 0},
        { 1500, 0, 0},
        { 200, 50, 0},
        { 100, 100, 0},
        { 0, 150, 0} },/*

        { { 0, 1000, 4095 },
        { 0, 200, 2000},
        { 200, 0, 200},
        { 100, 0, 50},
        { 100, 0, 0},

        { 0, 1300, 4095},
        { 0, 0, 1500},
        { 20, 0, 200},
        { 100, 0, 100},
        { 150, 0, 0} },

        { { 4095, 0, 0 },
        { 0, 0, 2000},
        { 0, 0, 500},
        { 0, 0, 300},
        { 0, 0, 100},

        { 4095, 1000, 0},
        { 0, 500, 1500},
        { 0, 250, 250},
        { 0, 150, 150},
        { 0, 0, 100} },

        { { 1000, 0, 3000 },
        { 200, 1500, 0},
        { 100, 400, 0},
        { 100, 100, 0},
        { 50, 50, 0},

        { 4095, 0, 0},
        { 0, 2000, 0},
        { 0, 500, 0},
        { 0, 200, 0},
        { 0, 110, 0} } */};

void Stage ( uint8_t number )
{
    uint8_t i;

    for (i=0; i<10; i++)
    {
        setLedRgb( i, stageModesFixeds[number][i][0], stageModesFixeds[number][i][1], stageModesFixeds[number][i][2]);
        //setLedRgb( i+10, stageModesFixeds[0][i][0], stageModesFixeds[0][i][1], stageModesFixeds[0][i][2]);
    }

    for (i=0; i<5; i++)
    {
        setLedRgb( i+15, stageModesFixeds[number][i][0], stageModesFixeds[number][i][1], stageModesFixeds[number][i][2]);
    }

    for (i=5; i<10; i++)
    {
        setLedRgb( i+5, stageModesFixeds[number][i][0], stageModesFixeds[number][i][1], stageModesFixeds[number][i][2]);
    }

    TLC5940_display();
}
