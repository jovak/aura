/**** A P P L I C A T I O N   N O T E   ************************************
*
* Title			: DMX512 reception library
* Version		: v1.3.1
* Last updated	: 30.03.13
* Target		: ATmega88
* Clock			: 8MHz, 16MHz
*
* written by hendrik hoelscher, www.hoelscher-hi.de
* (small changes to run on ATmega88 written by Jonathan Krieger)
***************************************************************************
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version2 of
 the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 If you have no copy of the GNU General Public License, write to the
 Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 For other license models, please contact the author.

;***************************************************************************/

#include "lib_dmx_in.h"

// ********************* local definitions *********************

enum {IDLE, BREAK, STARTB, STARTADR};			//DMX states

volatile uint8_t 	 gDmxState;


// *************** DMX Reception Initialisation ****************
void init_DMX_RX(void)
{

    DDRD  |= (1<<2);
    PORTD &= ~(1<<2);

    /* Set baud rate */
    UBRR0H = 0;  //enable reception
    UBRR0L = ((F_OSC_K/4000)-1);  //250kbaud, 8N2

    /* Enable receiver */
    UCSR0B = (1<<RXEN0)|(1<<RXCIE0);
    /* Set frame format: 8data (UCSZ01:0), 2stop bit (USBS0) */
    UCSR0C = (1<<USBS0)|(1<<UCSZ00)|(1<<UCSZ01);//|(1<<UMSEL00);
    gDmxState= IDLE;

}

// *************** DMX Reception ISR ****************
ISR (USART_RX_vect)
{
    static  uint16_t DmxCount;
    uint8_t  USARTstate= UCSR0A;				//get state before data!
    uint8_t  DmxByte   = UDR0;				//get data
    uint8_t  DmxState  = gDmxState;			//just load once from SRAM to increase speed

    if (USARTstate &(1<<FE0))						//check for break
    {
        UCSR0A &= ~(1<<FE0);							//reset flag (necessary for simulation in AVR Studio)
        DmxCount =  DmxAddress;						//reset channel counter (count channels before start address)
        gDmxState= BREAK;
    }
    else if (DmxState == BREAK)
    {
        if (DmxByte == 0)
        {
            gDmxState= STARTB;		//normal start code detected
        }
        else
        {
            gDmxState= IDLE;
        }

    }
    else if (DmxState == STARTB)
    {
        if (--DmxCount == 0)						//start address reached?
        {
            DmxCount= 1;							//set up counter for required channels
            DmxRxField[0]= DmxByte;					//get 1st DMX channel of device
            gDmxState= STARTADR;
        }
    }
    else if (DmxState == STARTADR)
    {
        DmxRxField[DmxCount++]= DmxByte;			//get channel
        if (DmxCount >= sizeof(DmxRxField)) 		//all ch received?
        {
            DmxFinishFlag = 1;
            gDmxState= IDLE;						//wait for next break
        }
    }
}

