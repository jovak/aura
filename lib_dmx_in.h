/**** A P P L I C A T I O N   N O T E   ************************************
*
* Title			: DMX512 reception library
* Version		: v1.3.1
* Last updated	: 30.03.13
* Target		: ATmega88
* Clock			: 8MHz, 16MHz
*
* written by hendrik hoelscher, www.hoelscher-hi.de
* (small changes to run on ATmega88 written by Jonathan Krieger)
***************************************************************************
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version2 of
 the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 If you have no copy of the GNU General Public License, write to the
 Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 For other license models, please contact the author.

;***************************************************************************/

#ifndef LIB_DMX_IN_H_
#define LIB_DMX_IN_H_

#include <avr/io.h>
#include <stdint.h>
#include <avr/interrupt.h>

//#define  USE_DIP
#define  F_OSC_K			(16000)		  		//oscillator freq. in kHz (typical 8MHz or 16MHz)

#define dmx_size 60						 // number of used DMX channels
#define BAUD 250000L					 // DMX512

#define UBRR_VAL  ((F_CPU+BAUD*8)/(BAUD*16)-1)
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD-1000)

#if ((BAUD_ERROR>10) || (BAUD_ERROR<-10))
#error Systematic baud error above 1% which is too high!
#endif


volatile uint8_t	 DmxRxField[dmx_size]; 		//array of DMX vals (raw)
volatile uint16_t	 DmxAddress;			//start addressDMX vals (raw)
volatile uint8_t	 DmxFinishFlag;			//

extern void    init_DMX_RX(void);
extern void    get_dips(void);

#endif /* LIB_DMX_IN_H_ */
