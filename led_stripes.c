/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/**
 * @file led_stripes.c
 *
 *  This file implements led stripes controll routines
 *
 * \author Jonathan Krieger
 */
/*-----------------------------------------------------------------------------------------------------------------------------------------------*/

#include "led_stripes.h"

const uint8_t stripe_conf[NR_OF_STRIPES][3] =
{
    { 61, 62, 60 }, // 132    4
    { 58, 59, 57 }, // 231 :  3
    { 55, 56, 54 }, // 213 .  2
    { 52, 53, 51 }, // 321 TLC5940 number of RGB at Stripe 1
    { 49, 50, 48 }, // 312 TLC5940 number of RGB at Stripe 0

    { 45, 46, 44 }, //     9
    { 42, 43, 41 }, //     8
    { 39, 40, 38 }, //     7
    { 36, 37, 35 }, //     6
    { 33, 34, 32 }, //     5

    { 29, 30, 28 }, //    14
    { 26, 27, 25 }, //    13
    { 23, 24, 22 }, //    12
    { 20, 21, 19 }, //    11
    { 17, 18, 16 }, //    10

    {  4,  5,  3 }, //    16
    {  7,  8,  6 }, //    17
    { 10, 11,  9 }, //    18
    { 13, 14, 12 },  //    19
    {  1,  2,  0 } //    15
};

// Table[(e^(n/8)-1)×87, {n, 0, 31}] calculatet@: http://www.wolframalpha.com/input/?i=Table[%28e**[n%2F8]-1%29*87%2C+{n%2C+0%2C+31}]
// last value changed to 4095 (the max. of the pwm value the TLC5940 chip can handle for each channel)
//const uint16_t brightness_5_to_12bit[32] = {0, 11, 24, 39, 56, 75, 97, 121, 149, 180, 216, 257, 302, 354, 413, 480, 555, 641, 738, 848, 972, 1114, 1273, 1455, 1660, 1893, 2156, 2455, 2794, 3177, 3612, 4095}

void setLed( uint8_t led_nr, uint16_t brightness ) //needs to be rewritten with stripe_conf
{
    if      ( led_nr >= 45 ) led_nr+=3;
    else if ( led_nr >= 30 ) led_nr+=2;
    else if ( led_nr >= 15 ) ++led_nr;
    TLC5940_setLED( led_nr, brightness );
}

void setLedRgb( uint8_t stripe_nr, uint16_t red, uint16_t green, uint16_t blue )
{
    TLC5940_setLED( stripe_conf[stripe_nr][0], red );
    TLC5940_setLED( stripe_conf[stripe_nr][1], green );
    TLC5940_setLED( stripe_conf[stripe_nr][2], blue );
}

void setAllLeds( uint16_t red, uint16_t green, uint16_t blue )
{
    uint8_t stripe_nr = 0;
    while (stripe_nr < NR_OF_STRIPES)

    {
        setLedRgb( stripe_nr, red, green, blue );
        ++stripe_nr;
    }
}
